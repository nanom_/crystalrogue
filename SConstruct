#!/usr/bin/env python3
import os
EnsurePythonVersion(3, 6)

projname = "crystalrogue"
target_platform = ARGUMENTS.get("platform", ARGUMENTS.get("p", "linux"))
build_type = ARGUMENTS.get("build", ARGUMENTS.get("b", "debug"))

if build_type not in ["debug", "release"]:
    build_type = "debug"

if target_platform in ["wasm", "webassembly", "html5", "javascript", "web"]:
    print("Don't be fooled by the stdout! WEBASSEMBLY BUILDS ARE BROKEN.")
    platform_string = "web"
    compiler = os.getenv(
        "EMSCRIPTEN_CXX",
        f"{os.getenv('HOME')}/.emsdk/upstream/emscripten/em++"
    )
    include_path = "./libs/raylib/webassembly/include"
    lib_path = "./libs/raylib/webassembly/lib"
    platform_libs = []
    binary = f"./build/{build_type}/html/{projname}.html"
elif target_platform in ["linux", "lin64", "lin", "unix", "nix", "gnu"]:
    platform_string = "linux 64 bit"
    compiler = "g++"
    include_path = "./libs/raylib/lin64/include"
    lib_path = "./libs/raylib/lin64/lib"
    platform_libs = ["GL", "m", "pthread", "dl", "rt", "X11"]
    binary = f"./build/{build_type}/{projname}.64"
elif target_platform in ["windows", "win64", "win"]:
    platform_string = "windows 64 bit"
    compiler = "x86_64-w64-mingw32-g++"
    include_path = "./libs/raylib/win64/include"
    lib_path = "./libs/raylib/win64/lib"
    platform_libs = ["pthread", "opengl32", "gdi32", "winmm"]
    binary = f"./build/{build_type}/{projname}.exe"
elif target_platform in ["linux32", "unix32", "lin32"]:
    print("32bit linux builds are currently not supported")
    exit(1)
    #target_platform = "linux 32 bit"
    #compiler = "g++ -m32"
    #include_path = "./libs/raylib/lin32/include"
    #lib_path = "./libs/raylib/lin32/lib"
    #binary = f"./build/{build_type}/{projname}.32"
elif target_platform in ["windows32", "win32"]:
    print("32bit windows builds are currently not supported")
    exit(1)
    #platform_string = "windows 32 bit"
    #compiler = "i686-w64-mingw32-g++"
    #include_path = "./libs/raylib/win32/include"
    #lib_path = "./libs/raylib/win32/lib"
    #platform_libs = ["glfw3", "opengl32", "gdi32", "openal32", "winmm"]
    #binary = f"./build/{build_type}/{projname}-x86.exe"

env = Environment(
    CXX = compiler,
    CXXFLAGS = ["-std=c++14"],
    CPPPATH = [include_path],
    LIBPATH = [lib_path],
    LIBS = [
        "raylib",
        platform_libs
    ],
)

if "windows" in platform_string:
    env.Append(LINKFLAGS = ['-static-libgcc', '-static-libstdc++'])
if "web" in platform_string:
    env.Append(LINKFLAGS = [
        '-sUSE_GLFW=3',
        '-sASYNCIFY',
        '-sFORCE_FILESYSTEM=1',
        '-Os',
        '-O2',
    ])

print(f"Building {projname} for {platform_string} ({build_type})")
env.Program(
    source = [Glob("src/*.cpp"), Glob(f"{lib_path}/*.a")],
    target = binary
)
