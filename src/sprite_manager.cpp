#include <assert.h>
#include "sprite_manager.hpp"

SpriteManager::SpriteManager()
{
	this->editMode = false;
	
	this->defaultPalette[0] = Color {15, 56, 15, 255};
	this->defaultPalette[1] = Color {48, 98, 48, 255};
	this->defaultPalette[2] = Color {139, 172, 15, 255};
	this->defaultPalette[3] = Color {155, 188, 15, 255};

	this->grayPalette[0] = Color {22, 22, 22, 255};
	this->grayPalette[1] = Color {106, 106, 106, 255};
	this->grayPalette[2] = Color {171, 171, 171, 255};
	this->grayPalette[3] = Color {255, 255, 255, 255};
}

SpriteManager::~SpriteManager()
{
}

void
SpriteManager::EnableEditMode(Color *palette)
{
	TraceLog(LOG_INFO, "SPRMNGR: Attempting to enable edit mode");
	this->spriteSheet = LoadImage(this->SPRITESHEET);
	for (int i = 0; i < 4; i++)
		ImageColorReplace(&this->spriteSheet,
			this->grayPalette[i],
			palette[i]);
	this->editMode = true;
}

void
SpriteManager::DisableEditMode()
{
	TraceLog(LOG_INFO, "SPRMNGR: Disabling edit mode");
	UnloadImage(this->spriteSheet);
	this->editMode = false;
}

void
SpriteManager::CreateSprite(const char *sprName,
	int numberOfFrames,
	FrameOptions *frames,
	int scale)
{
	assert(this->editMode);
	TraceLog(LOG_INFO,
		"SPRMNGR: Creating sprite '%s' (frames=%d)",
		sprName, numberOfFrames);

	Sprite sprite;
	for (int i = 0; i < numberOfFrames; i++) {
		Texture2D frameTexture;
		Image tmpImg = ImageCopy(this->spriteSheet);
		Rectangle crop;
		crop.x = this->FRAME_WIDTH * frames[i].x;
		crop.y = this->FRAME_HEIGHT * frames[i].y;
		crop.width = this->FRAME_WIDTH;
		crop.height = this->FRAME_HEIGHT;
		ImageCrop(&tmpImg, crop);
		if (frames[i].flipH)
			ImageFlipHorizontal(&tmpImg);
		if (frames[i].flipV)
			ImageFlipVertical(&tmpImg);
		ImageResizeNN(&tmpImg,
			this->FRAME_WIDTH * scale,
			this->FRAME_HEIGHT * scale);
		frameTexture = LoadTextureFromImage(tmpImg);
		sprite.CreateFrame(frameTexture);
		UnloadImage(tmpImg);
	}
	std::string sprNameStr(sprName);
	this->sprites.insert({sprNameStr, sprite});
}

void
SpriteManager::DeleteSprite(const char *sprName)
{
	assert(this->editMode);
	TraceLog(LOG_INFO, "SPRMNGR: Deleting sprite '%s'", sprName);

	std::string s(sprName);
	for (auto it = this->sprites.begin(); it != this->sprites.end(); it++) {
		if (!it->first.compare(s)) {
			for (int i = 0; i < it->second.FrameCount(); i++)
				UnloadTexture(*it->second.Get(i));
			this->sprites.erase(it);
		}
	}
}

Sprite *
SpriteManager::Get(const char *sprName)
{
	std::string s(sprName);
	return &this->sprites[s];
}
