#pragma once
#include <raylib.h>
#include <vector>

typedef struct SpriteState {
	int currentFrame;
} SpriteState;

class Sprite
{
public:
	Sprite();
	~Sprite();
	Texture2D *Get(int n);
	void CreateFrame(Texture2D frame);
	void DeleteFrame(int n);

	inline int FrameCount() {
		return frames.size();
	} 
private:
	std::vector<Texture2D> frames;
};
