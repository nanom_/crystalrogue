#pragma once
#include "tiles.hpp"
#include "coord.hpp"
#include <vector>

enum class RoomType
{
	EMPTY,
	START,
	END,
	KEY_ROOM,
	HALFWAY,
	NORMAL,
	WALL,
	TREASURE,
};

class Room
{
public:
	Room();
	~Room();
	void GenerateTiles();
	
	inline bool Initialized() {
		return initialized;
	}

	Coord pos;
	RoomType type;
	Room *parent;
	Room *neighbors[4];
	int f;
	int g;
	int h;
private:
	const int ROOM_WIDTH = 12;
	const int ROOM_HEIGHT = 8;
	bool initialized;
	std::vector<std::vector<RoomTile>> tiles;
};
