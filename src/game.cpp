#include <raylib.h>
#include <string>
#include "game.hpp"

Game::Game()
{
	InitWindow(
		this->WINDOW_WIDTH,
		this->WINDOW_HEIGHT,
		this->GAME_NAME
	);
	SetTargetFPS(this->TARGET_FPS);
	this->sprManager = new SpriteManager;
	this->map = new Map(10, 10, 8);

	this->sprManager->EnableEditMode(this->sprManager->DefaultPalette());
	FrameOptions frames[1];
	frames[0].x = 0;
	frames[0].y = 0;
	frames[0].flipH = false;
	frames[0].flipV = false;
	this->sprManager->CreateSprite("entities.player", 1, frames, 2);
	this->sprManager->DisableEditMode();

	TraceLog(LOG_INFO, "GAME: Starting main loop");
	this->MainLoop();
}

Game::~Game()
{
	this->sprManager->DeleteSprite("player1");

	delete map;
	delete sprManager;
	CloseWindow();
}

void
Game::MainLoop()
{
	while(1) {
		this->Input();
		this->Process();
		this->Draw();
	}
}

void
Game::Input()
{
	
}

void
Game::Process()
{
	if (!this->map->Ready2Play())
		this->map->GenerateNew();
}

void
Game::Draw()
{
	BeginDrawing();
	ClearBackground(Color {202, 220, 159});
	DrawTexture(*this->sprManager
		->Get("entities.player")
		->Get(0), 8, 8, WHITE);
	EndDrawing();
}
