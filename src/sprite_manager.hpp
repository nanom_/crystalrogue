#pragma once
#include "sprite.hpp"
#include <map>
#include <string>

typedef struct FrameOptions {
	int x;
	int y;
	bool flipH;
	bool flipV;
} FrameOptions;

class SpriteManager
{
public:
	SpriteManager();
	~SpriteManager();
	
	void EnableEditMode(Color *palette);
	void CreateSprite(const char *sprName,
		int numberOfFrames,
		FrameOptions *frames,
		int scale);
	void DeleteSprite(const char *sprName);
	void DisableEditMode();
	Sprite *Get(const char *sprName);

	inline Color *DefaultPalette() {
		return defaultPalette;
	}
private:
	const int FRAME_WIDTH = 16;
	const int FRAME_HEIGHT = 16;
	static constexpr const char *SPRITESHEET = "./data/sprites.png";
	Image spriteSheet;
	Color grayPalette[4];
	Color defaultPalette[4];
	std::map<std::string, Sprite> sprites;
	bool editMode;
};
