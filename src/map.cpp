#include <raylib.h>
#include <assert.h>
#include <cmath>
#include <algorithm>
#include "map.hpp"

Map::Map(int w, int h, int walls)
{
	this->width = w;
	this->height = h;
	this->walls = walls;
	this->ready = false;
	for(int y = 0; y < h; y++) {
		std::vector<Room> tmpV;
		for(int x = 0; x < w; x++) {
			Room r;
			r.type = RoomType::EMPTY;
			r.pos.x = x;
			r.pos.y = y;
			r.parent = nullptr;
			tmpV.push_back(r);
		}
		this->rooms.push_back(tmpV);
		tmpV.clear();
	}
}

Map::~Map()
{
}

void
Map::GenerateNew()
{
	assert(this->width >= 10 && this->height >= 10);
	assert(!this->ready);
	TraceLog(LOG_INFO, "MAPGEN: Generating rooms");
	this->RandomWallRooms();
	this->RandomRoomPositions();
	this->ConnectRooms(&this->start, &this->halfway);
	this->ResetAllParents();
	this->ConnectRooms(&this->halfway, &this->end);
	this->ResetAllParents();
	this->ConnectRooms(&this->halfway, &this->keyRoom);
	this->ResetAllParents();
	this->ConnectRooms(&this->start, &this->treasure);
	TraceLog(LOG_INFO, "MAPGEN: Generating tiles");
	this->FillRooms();
	this->ready = true;
	TraceLog(LOG_INFO, "MAPGEN: Map ready.");
}

void
Map::RandomWallRooms()
{
	for (int i = 0; i < this->walls; i++) {
		int ry = GetRandomValue(2, this->height - 3),
			rx = GetRandomValue(1, this->width - 1);
		this->rooms[ry][rx].type = RoomType::WALL;
	}
}

void
Map::RandomRoomPositions()
{
	this->start.y = this->height - 1;
	this->start.x = GetRandomValue(1, this->width - 2);
	this->end.y = 0;
	do {
		this->end.x = GetRandomValue(1, this->width - 2);
	} while (this->end.x == this->start.x);
	
	do {
		this->halfway.y = GetRandomValue(2, this->height - 4);
		this->halfway.x = GetRandomValue(1, this->width - 2);
	} while(
		this->rooms[this->halfway.y][this->halfway.x].type == RoomType::WALL
	);
		
	do {
		this->keyRoom.y = GetRandomValue(2, this->height - 3);
		this->keyRoom.x = GetRandomValue(1, this->width - 2);
	} while(
		abs(this->keyRoom.x - this->halfway.x) < 4 || 
		abs(this->keyRoom.y - this->halfway.y) < 3 || 
		this->rooms[this->keyRoom.y][this->keyRoom.x].type == RoomType::WALL
	);

	do {
		this->treasure.y = GetRandomValue(0, this->height - 4);
		this->treasure.x = GetRandomValue(1, this->width - 2);
	} while (
		(this->treasure.y == this->halfway.y &&
		this->treasure.x == this->halfway.x) ||
		(this->treasure.y == this->keyRoom.y && 
		this->treasure.x == this->keyRoom.x) ||
		this->rooms[this->treasure.y][this->treasure.x].type == RoomType::WALL
	);

	this->rooms[this->start.y][this->start.x].type = RoomType::START;
	this->rooms[this->end.y][this->end.x].type = RoomType::END;
	this->rooms[this->halfway.y][this->halfway.x].type = RoomType::HALFWAY;
	this->rooms[this->keyRoom.y][this->keyRoom.x].type = RoomType::KEY_ROOM;
	this->rooms[this->treasure.y][this->treasure.x].type = RoomType::TREASURE;
}

void
Map::ConnectRooms(Coord *start, Coord *end)
{
	Room *current;

	this->openSet.clear();
	this->closedSet.clear();
	int x2, x1, y2, y1;
	x2 = end->x;
	x1 = start->x;
	y2 = end->y;
	y1 = start->y;
	this->rooms[y1][x1].g = 0;
	this->rooms[y1][x1].h = abs(x2 - x1) + abs(y2 - y1);
	this->rooms[y1][x1].f = (this->rooms[y1][x1].g +
		this->rooms[y1][x1].h);
	this->openSet.push_back(&this->rooms[start->y][start->x]);

	int gVal;
	while(1) {
		gVal++;
		current = this->LowestFCostInOpenSet();
		this->closedSet.push_back(current);

		if (current->pos.x == end->x && current->pos.y == end->y) {
			this->ConnectParents(current);
			break;
		}
		
		auto cx = current->pos.x;
		auto cy = current->pos.y;
		current->neighbors[0] = cy > 0? &this->rooms[cy - 1][cx] : nullptr;
		current->neighbors[1] = (cy < this->height - 1? 
			&this->rooms[cy + 1][cx] : nullptr
		);
		current->neighbors[2] = cx > 0? &this->rooms[cy][cx - 1] : nullptr;
		current->neighbors[3] = (cx < this->width - 1? 
			&this->rooms[cy][cx + 1] : nullptr
		);

		for (int i = 0; i < 4; i++) {
			if (!current->neighbors[i])
				continue;
			if (std::count(
				this->closedSet.begin(),
				this->closedSet.end(),
				current->neighbors[i]
			) || current->neighbors[i]->type == RoomType::WALL)
				continue;
			if (!std::count(
				this->openSet.begin(),
				this->openSet.end(),
				current->neighbors[i]
			)) {
				x1 = current->neighbors[i]->pos.x;
				y1 = current->neighbors[i]->pos.y;
				current->neighbors[i]->g = gVal;
				current->neighbors[i]->h = abs(x2 - x1) + abs(y2 - y1);
				current->neighbors[i]->f = (current->neighbors[i]->g +
					current->neighbors[i]->h);
				current->neighbors[i]->parent = current;
				this->openSet.push_back(current->neighbors[i]);
			}
		}
	}
}

Room *
Map::LowestFCostInOpenSet()
{
	assert(!this->openSet.empty());
	Room *lowest = this->openSet[0];
	int i = 0, lowestFPos = 0;
	for (auto cell : this->openSet) {
		if (cell->f < lowest->f) {
			lowest = cell;
			lowestFPos = i;
		}
		i++;
	}
	this->openSet.erase(this->openSet.begin() + lowestFPos);
	return lowest;
}

void
Map::ConnectParents(Room *last)
{
	Room *current = last;
	while(current->parent) {
		if (current->type == RoomType::EMPTY)
			current->type = RoomType::NORMAL;
		current = current->parent;
	}
}

void
Map::ResetAllParents()
{
	for (int y = 0; y < this->height; y++)
	for (int x = 0; x < this->width; x++)
		this->rooms[y][x].parent = nullptr;
}

void
Map::FillRooms()
{
	for (int y = 0; y < this->height; y++)
	for (int x = 0; x < this->width; x++) {
		if (this->rooms[y][x].type != RoomType::EMPTY &&
			this->rooms[y][x].type != RoomType::WALL)
				this->rooms[y][x].GenerateTiles();
	}
}
