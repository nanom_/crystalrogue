#pragma once
#include <memory>
#include <vector>
#include "room.hpp"

class Map
{
public:
	Map(int w, int h, int walls);
	~Map();
	void GenerateNew();

	inline int Width() {
		return width;
	}
	inline int Height() {
		return height;
	}
	inline bool Ready2Play() {
		return ready;
	}
	inline std::vector<std::vector<Room>> *
	MapData() {
		return &rooms;
	}
private:
	void RandomRoomPositions();
	void RandomWallRooms();
	void ConnectRooms(Coord *start, Coord *end);
	Room *LowestFCostInOpenSet();
	void ConnectParents(Room *last);
	void ResetAllParents();
	void FillRooms();

	int width;
	int height;
	int walls;
	bool ready;
	Coord start;
	Coord end;
	Coord halfway;
	Coord keyRoom;
	Coord treasure;
	std::vector<Room *> openSet;
	std::vector<Room *> closedSet;
	std::vector<std::vector<Room>> rooms;
};
