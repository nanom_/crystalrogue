#include <assert.h>
#include "sprite.hpp"

Sprite::Sprite()
{
}

Sprite::~Sprite()
{
}

Texture2D *
Sprite::Get(int n)
{
	return &this->frames[n];
}

void
Sprite::CreateFrame(Texture2D frame)
{
	this->frames.push_back(frame);
}

void
Sprite::DeleteFrame(int n)
{
	UnloadTexture(this->frames[n]);
	this->frames.erase(this->frames.begin() + n);
}
