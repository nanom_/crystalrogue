#include <raylib.h>
#include "room.hpp"

Room::Room()
{	
	this->initialized = false;
}

Room::~Room()
{
}

void
Room::GenerateTiles()
{
	int warpUpX = 0,
		warpDownX = 0,
		warpLeftY = 0,
		warpRightY = 0;
	if (this->neighbors[0])
		warpUpX = GetRandomValue(1, this->ROOM_WIDTH - 2);
	if (this->neighbors[1])
		warpDownX = GetRandomValue(1, this->ROOM_WIDTH - 2);
	if (this->neighbors[2])
		warpLeftY = GetRandomValue(1, this->ROOM_HEIGHT - 2);
	if (this->neighbors[3])
		warpRightY = GetRandomValue(1, this->ROOM_HEIGHT - 2);
	
	for (int y = 0; y < this->ROOM_HEIGHT; y++) {
		std::vector<RoomTile> tmpV;
		for (int x = 0; x < this->ROOM_WIDTH; x++) {
			RoomTile t = RoomTile::EMPTY;
			if (!y)
				t = warpUpX && x == warpUpX? 
				RoomTile::EMPTY : RoomTile::BLOCK;
			if (y == this->ROOM_WIDTH - 1)
				t = warpDownX && x == warpDownX?
				RoomTile::EMPTY : RoomTile::BLOCK;
			if (!x)
				t = warpLeftY && y == warpLeftY?
				RoomTile::EMPTY : RoomTile::BLOCK;
			if (x == this->ROOM_WIDTH - 1)
				t = warpRightY && y == warpRightY?
				RoomTile::EMPTY : RoomTile::BLOCK;
			tmpV.push_back(t);
		}
		this->tiles.push_back(tmpV);
		tmpV.clear();
	}
	this->initialized = true;
}
