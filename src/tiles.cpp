#include "tiles.hpp"

Tileset::Tileset(SpriteManager *sprMngr)
{
	this->sprManager = sprMngr;
	this->InitTiles();
}

Tileset::~Tileset()
{
}

void
Tileset::InitTiles()
{
	this->sprManager->EnableEditMode(this->sprManager->DefaultPalette());
	this->sprManager->DeleteSprite("tiles.empty");
	this->sprManager->DeleteSprite("tiles.block");

	FrameOptions tEmptyFrames[1];
	tEmptyFrames[0].x = 0;
	tEmptyFrames[0].y = 2;
	tEmptyFrames[0].flipH = false;
	tEmptyFrames[0].flipV = false;
	this->sprManager->CreateSprite("tiles.empty", 1, tEmptyFrames, 2);
	this->tiles.insert({RoomTile::EMPTY, "tiles.empty"});

	FrameOptions tBlockFrames[1];
	tBlockFrames[0].x = 0;
	tBlockFrames[0].y = 3;
	tBlockFrames[0].flipH = false;
	tBlockFrames[0].flipV = false;
	this->sprManager->CreateSprite("tiles.block", 1, tBlockFrames, 2);
	this->tiles.insert({RoomTile::BLOCK, "tiles.block"});

	this->sprManager->DisableEditMode();
}

