#include "level.hpp"

Level::Level(SpriteManager *sprMngr)
{
	this->map = nullptr;
	this->tileset = new Tileset(sprMngr);
	this->currentRoom = {.x = -1, .y = -1};
}

Level::~Level()
{
	delete this->tileset;
}

void
Level::SetMap(Map *map)
{
	this->map = map;
}

void
Level::SetRoom(int roomX, int roomY)
{
	this->currentRoom.x = roomX;
	this->currentRoom.y = roomY;
}

void
Level::SetRoomCoord(Coord *roomCoord)
{
	this->currentRoom = *roomCoord;
}

void
Level::DrawRoom()
{
	if (!this->map)
		return;
	if (this->currentRoom.x == -1 || this->currentRoom.y == -1)
		return;
	
}
