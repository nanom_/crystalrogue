#pragma once
#include <map>
#include <string>
#include "sprite_manager.hpp"

enum class RoomTile
{
	EMPTY,
	BLOCK,
};

class Tileset
{
public:
	Tileset(SpriteManager *sprMngr);
	~Tileset();
private:
	void InitTiles();
	
	SpriteManager *sprManager;
	std::map<RoomTile, std::string> tiles;
};
