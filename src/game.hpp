#pragma once
#include "map.hpp"
#include "sprite_manager.hpp"

class Game
{
public:
	Game();
	~Game();

	static constexpr const char *GAME_NAME = "CrystalRogue";
	static const int WINDOW_WIDTH = 480;
	static const int WINDOW_HEIGHT = 360;
	static const int TARGET_FPS = 60;
	bool map_generated = false;
	Map *map;
	SpriteManager *sprManager;
private:
	void MainLoop();
	void Input();
	void Process();
	void Draw();
};
