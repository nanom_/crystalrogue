#pragma once
#include "map.hpp"
#include "coord.hpp"
#include "tiles.hpp"

class Level
{
public:
	Level(SpriteManager *sprMngr);
	~Level();
	void SetMap(Map *map);
	void SetRoom(int roomX, int roomY);
	void SetRoomCoord(Coord *roomCoord);
	void DrawRoom();

	inline Coord *CurrentRoom() {
		return &currentRoom;
	}
private:
	Map *map;
	Tileset *tileset;
	Coord currentRoom;
};
