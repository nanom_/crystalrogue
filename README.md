Crystal Rogue
===
This might become some sort of roguelike game some day. I've been developing 
this for the last month and I don't want to continue working on this project
for now. However, I'd like to share the source code because why not.
Remember, __this is not a game yet!__
But it might be useful for people who want to learn how to make games or
game engines using only C++ and raylib.

Compiling
---
You need to install SCons in order to compile this easily.
Also, you need GCC to compile for Linux and MinGW to compile for Windows.
WebAssembly builds are broken but if you want to fix that you need to install
Emscripten as well.
* To generate a binary for Linux run `scons` or `scons platform=linux`
* To generate a binary for Windows run `scons platform=windows`
* To generate a webassembly build run `scons platform=web`

Game licensed under the GPLv3 license, raylib licensed under the Zlib license.

